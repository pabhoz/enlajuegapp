import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { RegisterPage } from '../register/register';
import { HomePage } from '../home/home';
import { UsersProvider } from '../../providers/users/users';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  register: any = RegisterPage;
  email: string;
  password: string;
  
  constructor(public navCtrl: NavController, public navParams: NavParams, public toastCtrl: ToastController, private storage: Storage, private usersService: UsersProvider) {
  }

  ionViewDidLoad() {
    console.log(this.storage);
  }

  doLogin(){

    this.usersService.get(this.email).subscribe(
      (r)=> {
        if(this.email == r["email"] && this.password == r["password"]){
          this.storage.set("user",{id:r["id"],username:["username"]});
          this.navCtrl.setRoot(HomePage);
        }else{
          const toast = this.toastCtrl.create({
            message: 'Error, nombre de usuario o contraseña incorrecto.',
            duration: 3000,
            position: "top"
          });
          toast.present();
        }
      }
    );

    
  }

}
