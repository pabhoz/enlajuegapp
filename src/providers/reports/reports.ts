import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { API } from '../../globals';

/*
  Generated class for the UsersProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ReportsProvider {

  API = API;

  constructor(public http: HttpClient) {
    console.log('Hello ReportsProvider Provider');
  }
  get(id=null){
    let endpoint
    if(id == null){
       endpoint = this.API + "Report";
    }else{
      endpoint = this.API + `Report?id=${id}`;
    }
    
    return this.http.get(endpoint);
  }

  getNewOnes(last,q){
    let endpoint
    endpoint = this.API + `Report/NewOnes?last=${last}&q=${q}`;
    return this.http.get(endpoint);
  }

  create(body: any) {
    let endpoint = this.API + "User";
    let pbody = new FormData();

    pbody.append('username', body.username);
    pbody.append('password', body.password);
    pbody.append('email', body.email);
    pbody.append('age', body.age);
    pbody.append('sex', body.sex);
    pbody.append('phone', body.phone);

    //let headers = new Headers({});

    return this.http.post(endpoint, pbody);
  }

}
